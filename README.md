![icon](./icon-150x150.png)

# Project Branch Plus

The proposal for [*CUHK Golden Jubilee Professor Joseph J.Y. Sung Awards 2013-14*](https://www.iso.cuhk.edu.hk/english/publications/sustainable-campus/news.aspx?newsid=51).
It is originally created by InDesign and now is converted to a PDF. This repo is an archive of this project.

## Sketch

![Sketch](./overview.jpg)

